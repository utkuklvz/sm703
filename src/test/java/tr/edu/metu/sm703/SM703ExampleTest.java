package tr.edu.metu.sm703;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SM703ExampleTest {

    private SM703Example sm703Example = new SM703Example();

    @Test
    public void checkAdditionPos(){
        assertEquals(sm703Example.addition(5, 5), 10);
    }

    @Test
    public void checkAdditionPosNeg(){
        assertEquals(sm703Example.addition(-1, 2), 1);
    }

    @Test
    public void checkAdditionNeg(){
        assertEquals(sm703Example.addition(-5, -5), -10);
    }

    @Test
    public void checkMultipPos(){
        assertEquals(sm703Example.multiplication(5, 5), 25);
    }

    @Test
    public void checkMultipPosNeg(){
        assertEquals(sm703Example.multiplication(-1, 2), -2);
    }

    @Test
    public void checkMultipNeg(){
        assertEquals(sm703Example.multiplication(-5, -5), 25);
    }

    @Test
    public void checkMultipZero(){
        assertEquals(sm703Example.multiplication(0, 1), 0);
    }
}
